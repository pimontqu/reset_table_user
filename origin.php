<?php
class origin extends db
{
    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function get_all_user(){
        $query = 'SELECT * FROM mdl_user';
        $all_user = $this->dbConnect->query($query);
        return $all_user->fetchAll(PDO::FETCH_ASSOC);
    }
}