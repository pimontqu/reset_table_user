<?php
class destination extends db
{
    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function add_user($query_parts){
        $query = 'INSERT INTO mdl_user(' . $query_parts['fields'] . ') VALUES(' . $query_parts['values'] . ')';
        $result = $this->dbConnect->query($query);
        return array('query' => $query, 'result' => $result);
    }

    public function create_query(array $current_log){
        $query_parts = array('fields' => '', 'values' => '');
        foreach ($current_log as $name_field => $value_field) {
            $query_parts['fields'] .= $name_field . ',';
            $query_parts['values'] .= '\'' . str_replace('\'', ' ', $value_field) . '\',';
        }
        $query_parts['fields'] = substr($query_parts['fields'], 0, -1);
        $query_parts['values'] = substr($query_parts['values'], 0, -1);
        $query_parts['values'] = str_replace('\\', '\\\\', $query_parts['values']);
        return $query_parts;
    }
}