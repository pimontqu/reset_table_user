<?php
include_once 'db.php';
include_once 'origin.php';
include_once 'destination.php';

$info_db = parse_ini_file('db.ini', true);
$origin = new origin($info_db['origin']);
$dest = new destination($info_db['dest']);

$all_user = $origin->get_all_user();

foreach ($all_user as $user) {
    $part_query = $dest->create_query($user);
    $result = $dest->add_user($part_query);
    if(!$result['result']){
        $base_dir_log = 'logs/';
        $name_file_log = date('d-m-Y') . '.log';
        $path_log_file = $base_dir_log . $name_file_log;
        if(!file_exists($base_dir_log)){
            mkdir($base_dir_log);
        }
        $log_file  = fopen($path_log_file, 'a+');
        $log_message = '[' . date('H:i:s') . '] ' . $result['query'] . PHP_EOL;
        fwrite($log_file, $log_message);
    }
}
echo 'done well played';