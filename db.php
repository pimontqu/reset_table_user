<?php

class db
{
    protected $host;
    protected $pwd;
    protected $login;
    protected $db;
    protected $dbConnect;

    /**
     * connect to database
     */
    public function __construct($info_db) {
        $this->host = $info_db['host'];
        $this->db = $info_db['db'];
        $this->login = $info_db['user'];
        $this->pwd = $info_db['password'];
        try {
            $this->dbConnect = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db . ';charset=utf8', $this->login, $this->pwd);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
}